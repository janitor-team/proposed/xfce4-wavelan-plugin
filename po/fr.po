# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Charles Monzat <c.monzat@laposte.net>, 2018
# Gérald Barré <g.barre@free.fr>, 2006
# jc1 <jc1.quebecos@gmail.com>, 2013-2014
# Louis Moureaux <inactive+louis94@transifex.com>, 2014
# Louis Moureaux <inactive+louis94@transifex.com>, 2014
# Maximilian Schleiss <maximilian@xfce.org>, 2009
# Yannick Le Guen <leguen.yannick@gmail.com>, 2014-2015
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-03-05 00:32+0100\n"
"PO-Revision-Date: 2018-03-17 23:06+0000\n"
"Last-Translator: Charles Monzat <c.monzat@laposte.net>\n"
"Language-Team: French (http://www.transifex.com/xfce/xfce-panel-plugins/language/fr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fr\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../panel-plugin/wavelan.c:159 ../panel-plugin/wi_common.c:35
msgid "No carrier signal"
msgstr "Pas de porteuse"

#. Translators: net_name: quality quality_unit at rate Mb/s
#: ../panel-plugin/wavelan.c:173
#, c-format
msgid "%s: %d%s at %dMb/s"
msgstr "%s : %d %s à %d Mo/s"

#. Translators: quality quality_unit at rate Mb/s
#: ../panel-plugin/wavelan.c:176
#, c-format
msgid "%d%s at %dMb/s"
msgstr "%d %s à %d Mo/s"

#: ../panel-plugin/wavelan.c:180
msgid "No device configured"
msgstr "Aucun périphérique configuré"

#: ../panel-plugin/wavelan.c:501
msgid "Wavelan Plugin Options"
msgstr "Options du greffon Wavelan"

#: ../panel-plugin/wavelan.c:516
msgid "Properties"
msgstr "Propriétés"

#: ../panel-plugin/wavelan.c:527
msgid "Interface"
msgstr "Interface"

#: ../panel-plugin/wavelan.c:549
msgid "_Autohide when offline"
msgstr "Masquer _automatiquement hors connexion"

#: ../panel-plugin/wavelan.c:559
msgid "Autohide when no _hardware present"
msgstr "Masquer automatiquement en l’absence de _matériel"

#: ../panel-plugin/wavelan.c:570
msgid ""
"Note: This will make it difficult to remove or configure the plugin if there"
" is no device detected."
msgstr "Remarque : cette option rend difficile la configuration ou la suppression du greffon en l’absence de périphérique."

#: ../panel-plugin/wavelan.c:578
msgid "Enable sig_nal quality colors"
msgstr "Activer les couleurs de qualité de sig_nal"

#: ../panel-plugin/wavelan.c:589
msgid "Show _icon"
msgstr "Afficher l’_icône"

#: ../panel-plugin/wavelan.c:617 ../panel-plugin/wavelan.desktop.in.h:2
msgid "View the status of a wireless network"
msgstr "Afficher l’état d’un réseau sans fil"

#: ../panel-plugin/wi_common.c:38
msgid "No such WaveLAN device"
msgstr "Aucun périphérique WaveLAN"

#: ../panel-plugin/wi_common.c:41
msgid "Invalid parameter"
msgstr "Paramètre non valide"

#: ../panel-plugin/wi_common.c:44
msgid "Unknown error"
msgstr "Erreur inconnue"

#: ../panel-plugin/wi_linux.c:150
msgid "Unknown"
msgstr "Inconnu"

#: ../panel-plugin/wavelan.desktop.in.h:1
msgid "Wavelan"
msgstr "Wavelan"
