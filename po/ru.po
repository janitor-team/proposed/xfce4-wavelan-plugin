# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Sergey Alyoshin <alyoshin.s@gmail.com>, 2014-2015
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-03-05 00:32+0100\n"
"PO-Revision-Date: 2017-09-23 19:03+0000\n"
"Last-Translator: Sergey Alyoshin <alyoshin.s@gmail.com>\n"
"Language-Team: Russian (http://www.transifex.com/xfce/xfce-panel-plugins/language/ru/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ru\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: ../panel-plugin/wavelan.c:159 ../panel-plugin/wi_common.c:35
msgid "No carrier signal"
msgstr "Отсутствует несущий сигнал"

#. Translators: net_name: quality quality_unit at rate Mb/s
#: ../panel-plugin/wavelan.c:173
#, c-format
msgid "%s: %d%s at %dMb/s"
msgstr "%s: %d%s при %dМб/с"

#. Translators: quality quality_unit at rate Mb/s
#: ../panel-plugin/wavelan.c:176
#, c-format
msgid "%d%s at %dMb/s"
msgstr "%d%s при %dМб/с"

#: ../panel-plugin/wavelan.c:180
msgid "No device configured"
msgstr "Устройство не настроено"

#: ../panel-plugin/wavelan.c:501
msgid "Wavelan Plugin Options"
msgstr "Настройки модуля беспроводной сети"

#: ../panel-plugin/wavelan.c:516
msgid "Properties"
msgstr "Свойства"

#: ../panel-plugin/wavelan.c:527
msgid "Interface"
msgstr "Интерфейс"

#: ../panel-plugin/wavelan.c:549
msgid "_Autohide when offline"
msgstr "Скрывать при отсутствии си_гнала"

#: ../panel-plugin/wavelan.c:559
msgid "Autohide when no _hardware present"
msgstr "Скрывать при отсутствии ус_тройства"

#: ../panel-plugin/wavelan.c:570
msgid ""
"Note: This will make it difficult to remove or configure the plugin if there"
" is no device detected."
msgstr "Замечание: при этом будет сложно удалить или настроить модуль, если устройство не определено."

#: ../panel-plugin/wavelan.c:578
msgid "Enable sig_nal quality colors"
msgstr "Отображать ц_ветом качество сигнала"

#: ../panel-plugin/wavelan.c:589
msgid "Show _icon"
msgstr "Показывать з_начок"

#: ../panel-plugin/wavelan.c:617 ../panel-plugin/wavelan.desktop.in.h:2
msgid "View the status of a wireless network"
msgstr "Просмотр статуса беспроводной сети"

#: ../panel-plugin/wi_common.c:38
msgid "No such WaveLAN device"
msgstr "Нет такого устройства WaveLAN"

#: ../panel-plugin/wi_common.c:41
msgid "Invalid parameter"
msgstr "Неверный параметр"

#: ../panel-plugin/wi_common.c:44
msgid "Unknown error"
msgstr "Неизвестная ошибка"

#: ../panel-plugin/wi_linux.c:150
msgid "Unknown"
msgstr "Неизвестно"

#: ../panel-plugin/wavelan.desktop.in.h:1
msgid "Wavelan"
msgstr "Беспроводная сеть"
