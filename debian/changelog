xfce4-wavelan-plugin (0.6.1-2) UNRELEASED; urgency=medium

  * Trim trailing whitespace.
  * Use secure URI in Homepage field.
  * Update standards version to 4.4.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Fri, 17 Jan 2020 01:14:52 +0000

xfce4-wavelan-plugin (0.6.1-1) unstable; urgency=medium

  * New upstream version 0.6.1
  * d/control: update standards version to 4.4.0
  * use debhelper-compat b-deps for dh compat level
  * d/control: update dh compat level to 11

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 28 Aug 2019 13:56:34 +0200

xfce4-wavelan-plugin (0.6.0-2) unstable; urgency=medium

  * Moved the package to git on salsa.debian.org
  * Updated the maintainer address to debian-xfce@lists.debian.org
                                                           closes: #899747
  * d/gbp.conf added, following DEP-14
  * d/watch: use HTTPS protocol
  * New upstream version 0.6.0
  * run wrap-and-sort
  * d/control: drop duplicate section field
  * d/control: update standards version to 4.2.1
  * update dh compat to 10
  * d/control: drop Lionel from uploaders, thanks

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 08 Dec 2018 16:18:19 +0100

xfce4-wavelan-plugin (0.6.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - replace xfce4-panel-dev build-dep by libxfce4-panel-2.0-dev
    - replace libxfce4ui-1-dev build-dep by libxfce4ui-2-dev
    - drop build-dep on xfce4-dev-tools, automake and libtool.
    - update standards version to 3.9.8.
  * debian/rules:
    - drop call to xdt-autogen, config.{guess,sub} are ok now.
  * debian/copyright: drop obsolete URL.

 -- Yves-Alexis Perez <corsac@debian.org>  Mon, 21 Nov 2016 21:11:53 +0100

xfce4-wavelan-plugin (0.5.12-1) unstable; urgency=medium

  * New upstream release.

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 28 Jun 2015 13:57:32 +0200

xfce4-wavelan-plugin (0.5.11-3) unstable; urgency=low

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Yves-Alexis Perez ]
  * debian/control:
    - update standards version to 3.9.6.
    - add build-dep on xfce4-dev-tools, automake and libtool.
  * debian/rules:
    - call xdt-autogen before configure to regenerate config.{guess,sub} and
      gain support for newer architectures.                     closes: #765275

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 15 Oct 2014 22:27:37 +0200

xfce4-wavelan-plugin (0.5.11-2) unstable; urgency=low

  * debian/rules:
    - enable all hardening flags.
  * debian/control:
    - drop version in xfce4-panel-dev build-dep since stable has Xfce 4.8 now.
    - drop build-dep on dpkg-dev since stable one includes hardening support.
    - update standards version to 3.9.4.

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 26 May 2013 13:58:49 +0200

xfce4-wavelan-plugin (0.5.11-1) unstable; urgency=low

  * New upstream release.
  * Add a lintian override for hardening flags.

 -- Yves-Alexis Perez <corsac@debian.org>  Sat, 30 Jun 2012 15:45:51 +0200

xfce4-wavelan-plugin (0.5.10-1) unstable; urgency=low

  * New upstream release.
    - add a way to disable the icon.                            closes: #552765

 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 13 Apr 2012 20:56:15 +0200

xfce4-wavelan-plugin (0.5.8-1) unstable; urgency=low

  * New upstream release.
  * debian/rules:
    - fix removal of .la files.

 -- Yves-Alexis Perez <corsac@debian.org>  Mon, 09 Apr 2012 20:04:31 +0200

xfce4-wavelan-plugin (0.5.7-1) unstable; urgency=low

  * New upstream release.
  * debian/rules:
    - switch to dh(1).
    - use debhelper 9 and dpkg-dev 1.16.1 hardening support.
    - use multiarch paths
    - build with --parallel.
    - configure with --disable-static.
    - use find to delete .la files.
  * debian/control:
    - drop build-dep on cdbs and hardening-includes.
    - update debhelper build-dep to 9 and add build-dep on dpkg-dev 1.16.1.
    - replace libxfcegui4 build-dep by libxfce4ui.
    - update standards version to 3.9.3.
  * debian/compat bumped to 9.
  * debian/patches:
    - 02_fix-ftbfs-kfreebsd dropped, included upstream.

 -- Yves-Alexis Perez <corsac@debian.org>  Sun, 08 Apr 2012 15:52:54 +0200

xfce4-wavelan-plugin (0.5.6-1) unstable; urgency=low

  [ Yves-Alexis Perez ]
  * New upstream release.
  * debian/control:
    - add build-dep on libxfcegui4-dev.
    - remove Simon and Emanuele from uploaders, thanks to them.
    - update Xfce build-deps to 4.8.
    - update Standards-Version to 3.9.2.
  * debian/patches:
    - 01_fix-tooltips-gtk2.16 dropped, included upstream.
  * debian/rules:
    - pick {C,LD}FLAGS from dpkg-buildflags.
    - add -O1, -z,defs and --as-needed to LDFLAGS.
    - add hardening flags to {C,LD}FLAGS.
  * Switch to 3.0 (quilt) source format.

  [ Lionel Le Folgoc ]
  * debian/control:
    - add myself to Uploaders.

 -- Yves-Alexis Perez <corsac@debian.org>  Wed, 20 Apr 2011 10:21:10 +0200

xfce4-wavelan-plugin (0.5.5-3) unstable; urgency=low

  [ Evgeni Golov ]
  * Fix Vcs-* fields, they were missing 'trunk' in the path.

  [ Yves-Alexis Perez ]
  * debian/watch edited to track Xfce archive reorganisation.
  * debian/patches/
    - 02_fix-ftbfs-kfreebsd added, fix FTBFS on kfreebsd, thanks to
      Aurelien Jarno.
  * debian/control:
    - update standards version to 3.8.2.

 -- Yves-Alexis Perez <corsac@debian.org>  Thu, 13 Aug 2009 23:19:55 +0200

xfce4-wavelan-plugin (0.5.5-2) unstable; urgency=low

  * debian/patches/
    - 01_fix-tooltips-gtk2.16 added, fix tooltip not appearing on gtk 2.16.

 -- Yves-Alexis Perez <corsac@debian.org>  Mon, 25 May 2009 23:29:19 +0200

xfce4-wavelan-plugin (0.5.5-1) unstable; urgency=low

  * New upstream release.
  * debian/compat bumped to 7.
  * debian/control:
    - update debhelper build-dep to 7.
    - update standards version to 3.8.1.
    - remove Rudy and Martin from uploaders, thanks to them.
    - move to xfce section.
    - update build-deps to Xfce 4.6.
    - add build-dep on intltool.
    - add dep on ${misc:Depends}.
  * debian/patches:
    - 01_interface-length.patch dropped, merged upstream.
  * debian/rules:
    - -Wl,-z,defs -Wl,--as-needed added to LDFLAGS.


 -- Yves-Alexis Perez <corsac@debian.org>  Fri, 10 Apr 2009 08:18:54 +0200

xfce4-wavelan-plugin (0.5.4-2) unstable; urgency=low

  [ Simon Huggins ]
  * debian/control: Move fake Homepage field to a real one now dpkg
    supports it.
  * Add Vcs-* headers to debian/control

  [ Yves-Alexis Perez ]
  * debian/patches: 01_interface-length added (IFNAMSIZ == 16). closes: #466923
  * debian/control:
    - update my email address.
    - update standards version to 3.7.3.
  * debian/copyright: update dates and copyright holders.

 -- Yves-Alexis Perez <corsac@debian.org>  Tue, 29 Apr 2008 20:35:09 +0200

xfce4-wavelan-plugin (0.5.4-1) unstable; urgency=low

  (Yves-Alexis Perez)
  * New upstream release.
  * debian/control:
   - removed Jani from uploaders as he's not anymore in the team.
   - removed gtk from build-dep as it's pulled by xfce4-panel-dev anyway.

  (Stefan Ott)
  * debian/control: updated the url

  (Simon Huggins)
  * Build against latest and greatest xfce.

 -- Yves-Alexis Perez <corsac@corsac.net>  Mon, 16 Apr 2007 18:59:51 +0100

xfce4-wavelan-plugin (0.5.3-1) unstable; urgency=low

  * New upstream release.
  * Fixed parsing of /proc/net/wireless				Closes: #370496

 -- Yves-Alexis Perez <corsac@corsac.net>  Sat, 23 Sep 2006 15:41:40 +0100

xfce4-wavelan-plugin (0.5.0-2) unstable; urgency=low

  * Updated build-deps to 4.3.90.2 (Xfce 4.4 Beta2).

 -- Yves-Alexis Perez <corsac@corsac.net>  Wed, 26 Jul 2006 17:49:27 +0100

xfce4-wavelan-plugin (0.5.0-1) unstable; urgency=low

  * New upstream release
  * Set Standard policy version to 3.7.2

 -- Yves-Alexis Perez <corsac@corsac.net>  Sat, 03 Jun 2006 13:26:51 +0100

xfce4-wavelan-plugin (0.4.1-3) unstable; urgency=high

  * Binary skew blocks testing progression hence urgency high
  * Add build-depends on >= 4.2.2 (though this won't matter now the
    autobuilders have actually all installed it but hey)

 -- Simon Huggins <huggie@earth.li>  Thu, 14 Jul 2005 09:38:05 +0100

xfce4-wavelan-plugin (0.4.1-2) unstable; urgency=low

  * Rebuilding to fix libxfcegui dependency

 -- Emanuele Rocca <ema@debian.org>  Mon, 20 Jun 2005 21:27:32 +0200

xfce4-wavelan-plugin (0.4.1-1) unstable; urgency=low

  * Initial Release. (Closes: #273731)

 -- Rudy Godoy <rudy@kernel-panik.org>  Sat, 29 Jan 2005 18:03:46 -0500
